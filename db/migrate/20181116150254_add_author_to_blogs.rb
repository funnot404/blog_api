class AddAuthorToBlogs < ActiveRecord::Migration[5.2]
  def change
    add_reference :blogs, :author, index: true
    add_foreign_key :blogs, :users, column: :author_id

    add_reference :articles, :author, index: true
    add_foreign_key :articles, :users, column: :author_id
  end
end
