Rails.application.routes.draw do
  resources :blogs do
    resources :articles
  end

  resources :articles

  resources :users

  post 'signup', to: 'users#create'
  post 'signin', to: 'authentication#authenticate'
end
