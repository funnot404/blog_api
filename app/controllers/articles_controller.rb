class ArticlesController < ApplicationController
    def index
        json_response(Article.all)
    end
end
