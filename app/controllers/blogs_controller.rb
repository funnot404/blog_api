class BlogsController < ApplicationController
    before_action :set_blog, only: [:show, :update, :destroy]

    def index
        # @blogs = Blog.all
        @blogs = current_user.blogs
        json_response(@blogs)
    end

    def create
        @blog = current_user.blogs.create!(create_params)
        json_response(@blog)
    end

    def show
        json_response(@blog)
    end

    def update
        @blog.update!(update_params)
    end

    def destroy
        @blog.destroy
    end

    private

    def create_params
        params.permit(:title, :created_by)
    end

    def update_params
        params.permit(:title)
    end

    def set_blog
        @blog = current_user.blogs.find(params[:id])
    end
end
