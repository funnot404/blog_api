class UsersController < ApplicationController
    skip_before_action :authorize_request, only: :create
    
    def index
        json_response(User.all)
    end

    def create
        user = User.create!(create_param)
        auth_token = AuthenticateUser.new(user.email, user.password).call
        response = { message: Message.account_created, auth_token: auth_token }
        json_response(response, :created)
    end

    private 

    def create_param
        params.permit(
            :name,
            :email,
            :password,
            :password_confirmation
        )
    end
end
