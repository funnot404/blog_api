class Blog < ApplicationRecord
    has_many :articles, dependent: :destroy
    belongs_to :author, :class_name => "User"
    
    validates_presence_of :title
end
